package com.stefanini.hackaton.backend.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.stefanini.hackaton.backend.entity.ClientEntity;
import com.stefanini.hackaton.backend.repository.ClientRepository;
import com.stefanini.hackaton.backend.resource.ClientResource;

@Component
public class ClientService {
	
	@Autowired
	private ClientRepository clientRepository;

	public List<ClientResource> getClients(){
		List<ClientResource> result = new ArrayList<ClientResource>();
		Iterable<ClientEntity> iterable = clientRepository.findAll();
		iterable.forEach(item->{
			ClientResource resource = convertToResource(item);
			result.add(resource);
		});
		return result;
	}
	
	public void saveClient(ClientResource client){
		ClientEntity entity = convertToEntity(client);
		clientRepository.save(entity);
	}
	
	private ClientResource convertToResource(ClientEntity clientEntity){
		ClientResource resource = new ClientResource();
		resource.setId(clientEntity.getId());
		resource.setName(clientEntity.getName());
		resource.setUserName(clientEntity.getUserName());
		resource.setBirthDate(clientEntity.getBirthDate());
		resource.setStatus(clientEntity.getStatus());
		return resource;
	}

	private ClientEntity convertToEntity(ClientResource clientResource){
		ClientEntity entity = new ClientEntity();
		entity.setId(clientResource.getId());
		entity.setName(clientResource.getName());
		entity.setUserName(clientResource.getUserName());
		entity.setBirthDate(clientResource.getBirthDate());
		if(clientResource.getStatus() != null && !clientResource.getStatus().isEmpty()){
			entity.setStatus(clientResource.getStatus());
		}
		return entity;
	}

	public ClientResource getClientById(Integer clientId) {
		ClientEntity entity = clientRepository.findOne(clientId);
		return convertToResource(entity);
	}

}
