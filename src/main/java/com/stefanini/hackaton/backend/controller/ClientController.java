package com.stefanini.hackaton.backend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.stefanini.hackaton.backend.resource.ClientResource;
import com.stefanini.hackaton.backend.service.ClientService;

@RestController
@RequestMapping("/api")
public class ClientController {
	
	@Autowired
	private ClientService clientService;
	
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/client", method = RequestMethod.GET, produces = "application/json")
	public List<ClientResource> getClients(){
		List<ClientResource> clients = clientService.getClients();
		return clients;
	}
	
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/client", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public ResponseEntity<String> saveClient(@RequestBody ClientResource client){
		try{
			clientService.saveClient(client);
		}catch (Exception e){
			return ResponseEntity.ok(String.format("{ \"message\": \"%s\", \"type\": \"E\" }", e.getCause().getMessage()));
		}
		return ResponseEntity.ok("{ \"message\": \"Client saved\", \"type\": \"S\" }"); 
	}
	
	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/client", method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
	public ResponseEntity<String> updateClient(@RequestBody ClientResource client){
		try{
			clientService.saveClient(client);
		}catch (Exception e){
			return ResponseEntity.ok(String.format("{ \"message\": \"%s\", \"type\": \"E\" }", e.getCause().getMessage()));
		}
		return ResponseEntity.ok("{ \"message\": \"Client Updated\", \"type\": \"S\" }"); 
	}

	@CrossOrigin(origins = "*")
	@RequestMapping(value = "/client/{clientId}", method = RequestMethod.GET, produces = "application/json")
	public ClientResource getClientById(@PathVariable Integer clientId){
		ClientResource client = clientService.getClientById(clientId);
		return client;
	}
}
