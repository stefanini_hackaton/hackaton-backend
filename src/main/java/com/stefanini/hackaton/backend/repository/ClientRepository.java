package com.stefanini.hackaton.backend.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.stefanini.hackaton.backend.entity.ClientEntity;

@Repository
public interface ClientRepository extends CrudRepository<ClientEntity, Integer>{
	
}
