# README #

Esta é uma aplicação back-end feita em java para suportar um cadastro simples de clientes em uma base MySQL para utilização em Hackaton.

### What is this repository for? ###

Este repositório contém o código fonte da aplicação e as configurações necessárias para sua utilização. As tecnologias utilizadas neste projeto são:

* Spring-Boot
* Java 1.8
* JPA 2
* Hibernate
* MySQL

### How do I get set up? ###

Para esta aplicação, a execução é auto-contida e pode ser executada tanto pela IDE do eclipse quanto pela linha de comando.

* Através do Eclipse

Para executar pela IDE do eclipse, basta clicar com o botão direito do mouse no projeto e ir no menu: Run As -> Spring Boot App.

* Através da linha de comando

Para executar pela linha de comando, é necessário estar na pasta raiz do projeto e executar o comando:mvn spring-boot:run.
